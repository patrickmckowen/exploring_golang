package main

import (
    "github.com/jprichardson/readline-go"
    "fmt"
    "os"
)


func main() {
	readline.ReadLine(os.Stdin, func(line string) {
		fmt.Printf("Line: %s\n", line)
	})
}
