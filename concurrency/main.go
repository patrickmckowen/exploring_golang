// This file caintains code for testings the speed of golang
// and how goroutines can improve performance.
package main

import (
	"fmt"
	"time"
)

func main() {

	// First not concurrently
	run_calculation(1000, 1000, false)

	// Next concurrently
	run_calculation(1000, 1000, true)


	// Some more testing
	run_calculation(1000000, 1000, false)
	run_calculation(1000000, 1000, true)

	// This one is an interesting one! 
	// With the very small amount of calculations needed
	// The creation of goroutines actually makes the function
	// take longer than calling it without concurrency.
	run_calculation(10, 10, false)
	run_calculation(10, 10, true)

	// Testing creation of lots of go routines
	// These are not threads, these are much more 
	// lightweight so we can create tons of them
	// but how many before we start slowing back down?
	run_calculation(1000000, 1000000, false)
	run_calculation(1000000, 1000000, true)

	// Uncomment these if you have some time on your hands
	//run_calculation(1000000000, 1000, false)
	//run_calculation(1000000000, 1000, true)
}

// run_calculation takes the highest number in the range of numbers
// to be summed and the number of time you want to run the calculation
// and uses goroutines to concurrently sum all the numbers. A channel is
// used to ensure all goroutines complete.
func run_calculation(max, reps int, concurrently bool) {

	// Call the timing function using defer
	defer elapsed(fmt.Sprintf("Concurrent: true, Max: %d, Repetitions: %d, Concurrently: %t,", max, reps, concurrently))()

	// The user chooses whether to run the calculation concurrently
	if concurrently == true {

		// Create the needed channel
		c := make(chan int64)

		// Do the calculation some number of times 
		for i := 1; i < reps; i++ { go func(m int) { c <-calculation(m)}(max)	}

		// Make sure all go rountines finish
		for i := 1; i < reps; i++ { <-c	}

	} else {

		// Perform the calculation n(reps) number of times
		for i := 1; i < reps; i++ { calculation(max) }

	}

}

// calculation takes a number and sums every number up to it
func calculation(max int) int64 {
	var tot int64
	for i := 1; i < max; i++ { tot = tot + int64(i) }
	return tot
}

// elapsed is used for timing function execution.
// place it at the beggining of the inside of a function with
// a a defer statement before and a string you want to
// appear in the logged output.
// Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h"
func elapsed(what string) func() {
    start := time.Now()
    return func() { fmt.Printf("%s Took: %v\n", what, time.Since(start)) }
}

